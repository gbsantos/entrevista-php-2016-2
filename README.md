#### Instruções:
* Fazer todas as soluções deste teste em um fork deste repositório.
* Não é limitado o uso de tecnologias, apenas a linguagem: PHP.
* A avaliação é feita referente a padrões de código e qualidade das soluções.

#### Preparar os códigos abaixo (obrigatórios):
1. **Caixa eletrônico**: onde ao inserir um valor, terei o retorno da quantidade de notas para cada valor de nota (1, 2, 5, 10, 20, 50 e 100), tentando sempre retornar o menor número de notas possíveis. Exemplo:
   * Solicitei R$ 195,00; devo receber o retorno em notas:
     * 1 x R$ 100,00
     * 1 x R$ 50,00
     * 2 x R$ 20,00
     * 1 x R$ 5,00
2. **Busca de cliente**: Dada a base de dados na raíz desse repositório, faça um formulário e permita que eu busque por um e-mail e ele me mostre o valor do pedido para aquele e-mail em uma tabela em HTML.
3. **Contato**: Crie um formulário de contato com validação para os campos e envie esses dados por e-mail: 
    * Dados que devem estar contidos no formulário: Nome, e-mail, cpf e telefone/celular.
4. **API**: Dada uma requisição HTTP de tipo GET:
    * Imprimir na tela os dados abaixo:
        * Nome da empresa: top(node)
        * Site: topnode.com.br
        * Data atual:  d/m/Y H:i:s (valor variável)
        * URL: (caminho da URL solicitada)
    * Respeitando o formato solicitado na variável passada como parâmetro na URL de nome "**formato**" com as opções:
        * JSON
        * HTML
        * XML

#### Observação:
Se recebeu esse teste por indicação de alguém, não teremos o seu contato. Nos envie seus dados de contato no formulário em nosso site.