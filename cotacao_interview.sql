SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cotacao_interview`
--

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `email` varchar(120) NOT NULL,
  `cpf` char(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `email`, `cpf`) VALUES
(1, 'Nome A', 'phc+a@topnode.com.br', '28217339503'),
(2, 'Nome B', 'phc+b@topnode.com.br', '23267128897'),
(3, 'Nome C', 'phc+c@topnode.com.br', '44908333505'),
(4, 'Nome D', 'phc+d@topnode.com.br', '75752688639'),
(5, 'Nome E', 'phc+e@topnode.com.br', '81762287293');

-- --------------------------------------------------------

--
-- Table structure for table `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
  `id` int(11) NOT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `produto_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pedido`
--

INSERT INTO `pedido` (`id`, `created_at`, `cliente_id`, `produto_id`) VALUES
(1, '2016-06-15 00:00:00', 1, 1),
(2, '2016-06-15 00:00:00', 1, 1),
(3, '2016-06-15 00:00:00', 1, 2),
(4, '2016-06-15 00:00:00', 1, 3),
(5, '2016-06-15 00:00:00', 2, 3),
(6, '2016-06-15 00:00:00', 3, 2),
(7, '2016-06-15 00:00:00', 4, 1),
(8, '2016-06-15 00:00:00', 5, 2),
(9, '2016-06-15 00:00:00', 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produto`
--

INSERT INTO `produto` (`id`, `nome`) VALUES
(1, 'Master'),
(2, 'Blaster'),
(3, 'XPTO avançado');

-- --------------------------------------------------------

--
-- Table structure for table `produto_valor`
--

CREATE TABLE IF NOT EXISTS `produto_valor` (
  `id` int(11) NOT NULL,
  `valor` decimal(8,2) DEFAULT NULL,
  `idade_maxima` int(11) DEFAULT NULL,
  `idade_minima` int(11) DEFAULT NULL,
  `produto_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produto_valor`
--

INSERT INTO `produto_valor` (`id`, `valor`, `idade_maxima`, `idade_minima`, `produto_id`) VALUES
(1, '127.50', 18, 0, 1),
(2, '152.76', 23, 19, 1),
(3, '183.23', 28, 24, 1),
(4, '219.79', 33, 29, 1),
(5, '263.63', 38, 34, 1),
(6, '316.23', 43, 39, 1),
(7, '379.32', 48, 44, 1),
(8, '454.99', 53, 49, 1),
(9, '545.76', 58, 54, 1),
(10, '654.64', 999, 59, 1),
(21, '220.47', 18, 0, 2),
(22, '266.77', 23, 19, 2),
(23, '322.79', 28, 24, 2),
(24, '390.58', 33, 29, 2),
(25, '472.60', 38, 34, 2),
(26, '571.84', 43, 39, 2),
(27, '691.93', 48, 44, 2),
(28, '837.23', 53, 49, 2),
(29, '1013.05', 58, 54, 2),
(30, '1225.79', 999, 59, 2),
(31, '475.12', 18, 0, 3),
(32, '577.98', 23, 19, 3),
(33, '699.36', 28, 24, 3),
(34, '846.23', 33, 29, 3),
(35, '1023.93', 38, 34, 3),
(36, '1238.96', 43, 39, 3),
(37, '1499.14', 48, 44, 3),
(38, '1813.96', 53, 49, 3),
(39, '2194.89', 58, 54, 3),
(40, '2655.82', 999, 59, 3);

-- --------------------------------------------------------

--
-- Table structure for table `vidas`
--

CREATE TABLE IF NOT EXISTS `vidas` (
  `id` int(11) NOT NULL,
  `idade` int(11) DEFAULT NULL,
  `cliente_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vidas`
--

INSERT INTO `vidas` (`id`, `idade`, `cliente_id`) VALUES
(1, 23, 1),
(2, 59, 2),
(3, 45, 2),
(4, 60, 2),
(5, 18, 3),
(6, 17, 3),
(7, 69, 4),
(8, 47, 5),
(9, 44, 5),
(10, 10, 5),
(11, 14, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pedido_cliente1_idx` (`cliente_id`),
  ADD KEY `fk_pedido_produto1_idx` (`produto_id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto_valor`
--
ALTER TABLE `produto_valor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_produto_valor_produto1_idx` (`produto_id`);

--
-- Indexes for table `vidas`
--
ALTER TABLE `vidas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vidas_cliente_idx` (`cliente_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `produto_valor`
--
ALTER TABLE `produto_valor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `vidas`
--
ALTER TABLE `vidas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_pedido_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pedido_produto1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `produto_valor`
--
ALTER TABLE `produto_valor`
  ADD CONSTRAINT `fk_produto_valor_produto1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vidas`
--
ALTER TABLE `vidas`
  ADD CONSTRAINT `fk_vidas_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
